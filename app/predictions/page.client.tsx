"use client";

import { type FormEvent } from "react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { z } from "zod";
import Link from "next/link";

import { PredictionSchema } from "./schema";

const createPrediction = async (prompt: string) => (
  fetch("/api/predictions", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        prompt,
      }),
    }).then((res) => {
      if (res.status !== 201) {
        throw new Error(res.statusText);
      }
      return res.json()
    })
)

export default function Predictions() {
  const router = useRouter();

  const { data, error } = useQuery({
    queryKey: ['predictions'],
    queryFn: async () => {
      const res = await fetch("/api/predictions")
      const ResSchema = z.object({
        results: z.array(PredictionSchema),
      })
      const d = await res.json()
      console.log('res', d)
      return ResSchema.parse(d).results
    },
    // TODO add polling (slow polling though since this is the full list of generated images)
    // enabled: !!slug && intervalMs > 0,
    // refetchInterval: intervalMs,
  })

  const mutation = useMutation({
    mutationFn: createPrediction,
    onSuccess: () => {
        router.push(`/predictions/${prediction.id}`);
      },
    // TODO
    // onSuccess: () => queryClient.invalidateQueries({ queryKey: ['todos'] }),
  });
  const prediction = mutation.data;

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    mutation.mutate(e.currentTarget.prompt.value);
  };

  console.log('data', data)

  // TODO add proper form to control all the parameters of the prediction
  return (
    <div className="mx-4 w-full">
      <h1 className="text-3xl mb-8 pt-12">Predictions</h1>
      <p className="mb-8">
        Dream something with{" "}
        <a href="https://replicate.com/stability-ai/stable-diffusion">SDXL</a>:
      </p>

      <form className="dark:text-slate-300 mb-12 flex gap-4" onSubmit={handleSubmit}>
        <input className="dark:bg-gray-900" type="text" name="prompt" placeholder="Enter a prompt to display an image" />
        <button className="px-4 bg-gray-300 dark:bg-gray-900" type="submit">Go!</button>
      </form>

      {mutation.isLoading && <div>Loading...</div>}
      {mutation.isError && (
        <div>Mutation error: {String(mutation.error)}</div>
      )}

      {error && <div>All fetch failed with: {String(error)}</div>}
      {data != null && (
        <>
          <div className="mb-4">Already done predictions: {data?.length}</div>
          <ul>
            {data?.map((prediction) => (
              <li>
                <Link className="underline" href={`/predictions/${prediction.id}`}>
                {prediction.id} : {prediction.status}
                </Link>
              </li>
            ))}
          </ul>
        </>
      )}
    </div>
  );
}
