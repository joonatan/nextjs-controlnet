import Client from './slug.client'

// TODO try to get the initial data from the server
export default async function Predictions({ params: { slug } }: { params: { slug: string } }) {
  // const initialData = await getPosts()

  console.log('page with slug', slug)

  return <Client slug={slug} />
}

