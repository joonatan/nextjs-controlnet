"use client";

import React from "react";
import { useQuery } from "@tanstack/react-query";
import Image from "next/image";

import { PredictionSchema } from "../schema";

const REFETCH_INTERVAL_MS = 10 * 1000;
// TODO allow stopping the refetches from UI
export default function Prediction({ slug }: { slug: string }) {
  const [intervalMs, setIntervalMs] = React.useState(-1) // REFETCH_INTERVAL_MS) // 

  // TODO make this into a polling function till status === succeeded
  const { data, error, isFetching } = useQuery({
    queryKey: ['predictions', slug],
    queryFn: async () => {
      const res = await fetch("/api/predictions/" + slug)
      const d = await res.json().then((x) => PredictionSchema.parse(x))
      if (d.status === 'succeeded') {
        console.log('finished processing: ', d)
        setIntervalMs(-1)
      }
      return d
    },
    enabled: !!slug && intervalMs > 0,
    refetchInterval: intervalMs,
  })

  if (isFetching) {
    return <div>Loading...</div>
  }
  if (error) {
    return <div>Fetch Error: {String(error)}</div>
  }
  // TODO refetch till we have data
  // TODO check if the data is still being generated
  if (!data) {
    return <div>no data</div>
  }

  console.log(data)
  // TODO print the other data not just not having output
  return (
    <div>
      <div>id: {data.id}</div>
      <div>Status: {data.status}</div>
      <div>Created at: {data.created_at}</div>
      <div>Started at: {data.started_at}</div>
      <div>Completed at: {data.completed_at}</div>
      <div>Source: {data.source}</div>
      <div>Input propmt: {data.input.prompt}</div>
      {data.output && (
        <div>
          <Image
            fill
            src={data.output[data.output.length - 1]}
            alt="output"
            sizes='100vw'
          />
          </div>
      )}
      <p>status: {data.status}</p>
    </div>
  )
}
