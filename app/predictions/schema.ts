import { z } from "zod";

export const PredictionSchema = z.object({
  error: z.unknown().nullish(),
  id: z.string(),
  input: z.object({
    prompt: z.string().optional(),
  }),
  logs: z.string().nullish(),
  created_at: z.string(),
  started_at: z.string(),
  completed_at: z.string().nullable(),
  source: z.enum(['api', 'cli', 'web']),
  status: z.enum(['starting', 'processing', 'succeeded', 'failed', 'canceled']),
  /*
    starting: the prediction is starting up. If this status lasts longer than a few seconds, then it's typically because a new worker is being started to run the prediction.
    processing: the predict() method of the model is currently running.
    succeeded: the prediction completed successfully.
    failed: the prediction encountered an error during processing.
    canceled: the prediction was canceled by the user.
  */
  urls: z.object({
    cancel: z.string(),
    get: z.string(),
  }),
  version: z.string(),
  output: z.array(z.string()).nullable(),
  metrics: z.object({
    predict_time: z.number(),
  }).nullable(),
  webhook_completed: z.string().nullish(),
}).passthrough();
