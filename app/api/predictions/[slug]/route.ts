import { NextRequest, NextResponse } from "next/server";

export async function GET(_req: NextRequest,
  { params }: { params: { slug: string } }
) {
  const { slug } = params
  if (!slug) {
    return NextResponse.json({ error: 'Slug is required' }, { status: 400 });
  }
  // TODO env validation should be done outside of the route
  if (!process.env.REPLICATE_API_TOKEN) {
    return NextResponse.json({ error: 'Replicate API token is required' }, { status: 400 });
  }

  console.log('Getting a prediction with slug: ', slug)
  const response = await fetch(
    `https://api.replicate.com/v1/predictions/${slug}`,
    {
      headers: {
        Authorization: `Token ${process.env.REPLICATE_API_TOKEN}`,
        "Content-Type": "application/json",
      },
    }
  )
  if (response.status !== 200) {
    let error = await response.json();
    return NextResponse.json({ error: error.detail }, { status: 500 });
  }

  const prediction = await response.json();
  return NextResponse.json(prediction, { status: 200 });
}
