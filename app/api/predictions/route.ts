import { NextResponse, type NextRequest } from "next/server";

export async function POST(req: NextRequest) {
  const body = await req.json()
  console.log('Making a prediction with body: ', body)
  if (!body.prompt) {
    return NextResponse.json({ error: 'Prompt is required' }, { status: 400 });
  }
  const response = await fetch("https://api.replicate.com/v1/predictions", {
    method: "POST",
    headers: {
      Authorization: `Token ${process.env.REPLICATE_API_TOKEN}`,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      // Pinned to a specific version of Stable Diffusion
      // See https://replicate.com/stability-ai/sdxl
      // version: "2b017d9b67edd2ee1401238df49d75da53c523f36e363881e057f5dc3ed3c5b2",
      version: "d830ba5dabf8090ec0db6c10fc862c6eb1c929e1a194a5411852d25fd954ac82",

      // This is the text prompt that will be submitted by a form on the frontend
      input: { prompt: body.prompt },
    }),
  });

  if (response.status !== 201) {
    let error = await response.json();
    console.error('Error: ' + error.detail);
    // res.end(JSON.stringify({ detail: error.detail }));
    return NextResponse.json({ error: error.detail }, { status: 500 });
  }

  const prediction = await response.json();
  // res.statusCode = 201;
  // res.end(JSON.stringify(prediction));
  // return res.status(201)
  return NextResponse.json(prediction, { status: 201 });
}

export async function GET(_req: NextRequest) {
  const response = await fetch("https://api.replicate.com/v1/predictions", {
    method: "GET",
    headers: {
      Authorization: `Token ${process.env.REPLICATE_API_TOKEN}`,
      "Content-Type": "application/json",
    },
  })

  if (response.status !== 200) {
    let error = await response.json();
    console.error('Error: ' + error.detail);
    return NextResponse.json({ error: error.detail }, { status: 500 });
  }

  const prediction = await response.json();
  console.log('prediction: ', prediction)
  return NextResponse.json(prediction, { status: 200 });
}
